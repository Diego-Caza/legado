$(function () {
    $('[data-toggle="tooltip"]').tooltip()
    $('.carousel').carousel({
        interval: 2000
    });

    $('#contacto').on('show.bs.modal', function (e){
      console.log('El modal se abre');
      $('#contactoBtn').removeClass('btn-outline-success');
      $('#contactoBtn').addClass('btn-primary');
      $('#contactoBtn').prop('disabled', true);
    });
    $('#contacto').on('shown.bs.modal', function (e){
      console.log('El modal se abrio');
    });
    $('#contacto').on('hide.bs.modal', function (e){
      console.log('El modal se cierra');
      $('#contactoBtn').removeClass('btn-primary');
      $('#contactoBtn').addClass('btn-outline-success');
    });
    $('#contacto').on('hidden.bs.modal', function (e){
      console.log('El modal se cerro');
      $('#contactoBtn').prop('disabled', false);
    });
  });